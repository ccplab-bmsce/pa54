#include<stdio.h>

int main()
{
   int marks[5][5],i,j,n,m,max_marks;
   printf("Enter the number of students\n");
   scanf("%d",&n);
   printf("Enter the number of subjects\n");
   scanf("%d",&m);
   for(i=1;i<=n;i++)
   { printf("Enter the marks of student i in m subjects\n");
     for(j=1;j<=m;j++)
     scanf("%d",&marks[i][j]);
   }
     
     for(j=1;j<=m;j++)
     { max_marks=-999;
       for(i=1;i<=n;i++)
       { 
         if(marks[i][j]>max_marks)
         max_marks=marks[i][j];
       }
       printf("Highest marks of student %d=%d\n",j,max_marks);
     }  
   return 0;
}